//! Includes
#define __SCROLL_IMPL__
#include "EmptyOrxScroll.h"
#undef __SCROLL_IMPL__

orxSTATUS EmptyOrxScroll::Init ()
{
    orxSTATUS result = orxSTATUS_SUCCESS;

    return result;
}

orxSTATUS EmptyOrxScroll::Run ()
{
    orxSTATUS result = orxSTATUS_SUCCESS;

    return result;
}

void EmptyOrxScroll::Exit ()
{
}

void EmptyOrxScroll::BindObjects ()
{
}

int main (int argc, char **argv)
{
  // Executes game
  EmptyOrxScroll::GetInstance ().Execute (argc, argv);

  // Done!
  return EXIT_SUCCESS;
}

#ifdef __orxWINDOWS__

#include "windows.h"

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
  // Executes game
  EmptyOrxScroll::GetInstance ().Execute ();

  // Done!
  return EXIT_SUCCESS;
}

#endif // __orxWINDOWS__

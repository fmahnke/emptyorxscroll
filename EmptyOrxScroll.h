#ifndef __EmptyOrxScroll_H_
#define __EmptyOrxScroll_H_

//! Includes
#define __NO_SCROLLED__ // Uncomment this define to prevent the embedded editor (ScrollEd) from being compiled
#include "Scroll.h"

//! EmptyOrxScroll class
class EmptyOrxScroll : public Scroll<EmptyOrxScroll>
{
public:

private:
    virtual orxSTATUS Init ();
    virtual orxSTATUS Run ();
    virtual void      Exit ();
    virtual void      BindObjects ();
};

#endif // __EmptyOrxScroll_H_
